package com.ferit.landekam.gametimetracker;

public class GameSession {

    private String SessionHours;
    private String SessionMinutes;
    private String SessionDate;

    public GameSession(){

    }

    public GameSession(String sessionHours, String sessionMinutes, String sessionDate){
        SessionHours = sessionHours;
        SessionMinutes = sessionMinutes;
        SessionDate = sessionDate;
    }

    public String GetSessionGametimeHoursAndMinutes(){
        String gametimeHoursString, gametimeMintuesString;
        if (Integer.valueOf(SessionMinutes) < 10){
            gametimeMintuesString = "0" + SessionMinutes;
        }
        else {
            gametimeMintuesString = SessionMinutes;
        }
        if (Integer.valueOf(SessionHours) < 10){
            gametimeHoursString = "0" + SessionHours;
        }
        else {
            gametimeHoursString = SessionHours;
        }
        String sessionGametimeString = gametimeHoursString + ":" + gametimeMintuesString;
        return sessionGametimeString;
    }

    public String GetDate(){
        return SessionDate;
    }
}
