package com.ferit.landekam.gametimetracker;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ActivityStatsGame extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private TextView textViewGameTitle, textViewNumberMain, textViewAverageMain,
            textViewNumberCompletitionist, textViewNumberSpeedrun, textViewNumberTotal,
            textViewAverageCompletitionist, textViewAverageSpeedrun, textViewAverageTotal;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private String GameId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_game);
        GameId = getIntent().getStringExtra("Id");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        textViewGameTitle = findViewById(R.id.textViewGameTitle);
        textViewNumberMain = findViewById(R.id.textViewNumberMain);
        textViewNumberCompletitionist = findViewById(R.id.textViewNumberCompletitionist);
        textViewNumberSpeedrun = findViewById(R.id.textViewNumberSpeedrun);
        textViewNumberTotal = findViewById(R.id.textViewNumberTotal);
        textViewAverageMain = findViewById(R.id.textViewAverageMain);
        textViewAverageCompletitionist = findViewById(R.id.textViewAverageCompletitionist);
        textViewAverageSpeedrun = findViewById(R.id.textViewAverageSpeedrun);
        textViewAverageTotal = findViewById(R.id.textViewAverageTotal);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Query query = databaseReference.child("games").child(GameId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                textViewGameTitle.setText(dataSnapshot.child("name").getValue().toString());
                int countMain = 0, countCompletitionist = 0, countSpeedrun = 0, countTotal = 0;
                int minutesMain = 0, minutesCompletitionist = 0, minutesSpeedrun = 0, minutesTotal = 0;
                int hoursMain = 0, hoursCompletitionist = 0, hoursSpeedrun = 0, hoursTotal = 0;
                int minutesMainAveage = 0, minutesCompletitionistAveage = 0,
                        minutesSpeedrunAveage = 0, minutesTotalAveage = 0;

                for(DataSnapshot snapshot : dataSnapshot.child("playthroughs").getChildren()){
                    if(snapshot.child("playstyle").getValue().toString().equals("Main game")){
                        countMain++;
                        String s = snapshot.child("minutes").getValue().toString();
                        minutesMain += Integer.parseInt(snapshot.child("minutes").getValue().toString());
                        minutesMain += Integer.parseInt(snapshot.child("hours").getValue().toString()) * 60;
                    }
                    else if(snapshot.child("playstyle").getValue().toString().equals("Completitionist")){
                        countCompletitionist++;
                        minutesCompletitionist += Integer.parseInt(snapshot.child("minutes").getValue().toString());
                        minutesCompletitionist += Integer.parseInt(snapshot.child("hours").getValue().toString()) * 60;
                    }
                    else {
                        countSpeedrun++;
                        minutesSpeedrun += Integer.parseInt(snapshot.child("minutes").getValue().toString());
                        minutesSpeedrun += Integer.parseInt(snapshot.child("hours").getValue().toString()) * 60;
                    }
                    countTotal++;
                    minutesTotal += Integer.parseInt(snapshot.child("minutes").getValue().toString());
                    minutesTotal += Integer.parseInt(snapshot.child("hours").getValue().toString()) * 60;
                }

                if(countMain > 0){
                    minutesMainAveage = minutesMain / countMain;
                }
                if(countCompletitionist > 0){
                    minutesCompletitionistAveage = minutesCompletitionist / countCompletitionist;
                }
                if(countSpeedrun > 0){
                    minutesSpeedrunAveage = minutesSpeedrun / countSpeedrun;
                }
                if(countTotal > 0){
                    minutesTotalAveage = minutesTotal / countTotal;
                }




                minutesMain = minutesMainAveage % 60;
                hoursMain = minutesMainAveage / 60;
                minutesCompletitionist = minutesCompletitionistAveage % 60;
                hoursCompletitionist = minutesCompletitionistAveage / 60;
                minutesSpeedrun = minutesSpeedrunAveage % 60;
                hoursSpeedrun = minutesSpeedrunAveage / 60;
                minutesTotal = minutesTotalAveage % 60;
                hoursTotal = minutesTotalAveage / 60;

                String hoursMainS, hoursCompletitionistS, hoursSpeedrunS, hoursTotalS,
                        minutesMainS, minutesCompletitionistS, minutesSpeedrunS, minutesTotalS;

                if(minutesMain < 10){
                    minutesMainS = "0" + minutesMain;
                }
                else {
                    minutesMainS = "" + minutesMain;
                }
                if(hoursMain < 10){
                    hoursMainS = "0" + hoursMain;
                }
                else {
                    hoursMainS = "" + hoursMain;
                }
                if(minutesCompletitionist < 10){
                    minutesCompletitionistS = "0" + minutesCompletitionist;
                }
                else {
                    minutesCompletitionistS = "" + minutesCompletitionist;
                }
                if(hoursCompletitionist < 10){
                    hoursCompletitionistS = "0" + hoursCompletitionist;
                }
                else {
                    hoursCompletitionistS = "" + hoursCompletitionist;
                }
                if(minutesSpeedrun < 10){
                    minutesSpeedrunS = "0" + minutesSpeedrun;
                }
                else {
                    minutesSpeedrunS = "" + minutesSpeedrun;
                }
                if(hoursSpeedrun < 10){
                    hoursSpeedrunS = "0" + hoursSpeedrun;
                }
                else {
                    hoursSpeedrunS = "" + hoursSpeedrun;
                }
                if(minutesTotal < 10){
                    minutesTotalS = "0" + minutesTotal;
                }
                else {
                    minutesTotalS = "" + minutesTotal;
                }
                if(hoursTotal < 10){
                    hoursTotalS = "0" + hoursTotal;
                }
                else {
                    hoursTotalS = "" + hoursTotal;
                }

                String hoursAndMinutesMain, hoursAndMinutesCompletitionist, hoursAndMinutesSpeedrun,
                        hoursAndMinutesTotal;
                hoursAndMinutesMain = hoursMainS + ":" + minutesMainS;
                hoursAndMinutesCompletitionist = hoursCompletitionistS + ":" + minutesCompletitionistS;
                hoursAndMinutesSpeedrun = hoursSpeedrunS + ":" + minutesSpeedrunS;
                hoursAndMinutesTotal = hoursTotalS + ":" + minutesTotalS;

                textViewNumberMain.setText(String.valueOf(countMain));
                textViewNumberCompletitionist.setText(String.valueOf(countCompletitionist));
                textViewNumberSpeedrun.setText(String.valueOf(countSpeedrun));
                textViewNumberTotal.setText(String.valueOf(countTotal));
                textViewAverageMain.setText(hoursAndMinutesMain);
                textViewAverageCompletitionist.setText(hoursAndMinutesCompletitionist);
                textViewAverageSpeedrun.setText(hoursAndMinutesSpeedrun);
                textViewAverageTotal.setText(hoursAndMinutesTotal);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
