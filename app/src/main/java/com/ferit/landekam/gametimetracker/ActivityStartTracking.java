package com.ferit.landekam.gametimetracker;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

public class ActivityStartTracking extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private Button buttonAddNewGame;
    private ListView listViewCurrentlyPlaying;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ArrayList<UserGamePlaythrough> listOfPlaythroughs = new ArrayList<>();
    private ChildEventListener childEventListener;
    private Query queryPlaythroughs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_tracking);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid());
        InitializeUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        InitializeList();
    }

    private void InitializeUI() {
        buttonAddNewGame = findViewById(R.id.buttonAddNewGame);
        imageButtonBack = findViewById(R.id.imageButtonBack);
        listViewCurrentlyPlaying = findViewById(R.id.listViewCurrentlyPlaying);


        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonAddNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartAddNewGameActivity();
            }
        });




    }

    private void StartAddNewGameActivity() {
        Intent intent = new Intent(this, ActivityAddNewGame.class);
        queryPlaythroughs.removeEventListener(childEventListener);
        startActivity(intent);
    }

    private void InitializeList(){
        final PlaythroughAdapter playthroughAdapter = new PlaythroughAdapter(this, listOfPlaythroughs);
        listViewCurrentlyPlaying.setAdapter(playthroughAdapter);
        listOfPlaythroughs.clear();
        playthroughAdapter.notifyDataSetChanged();
        Toast.makeText(ActivityStartTracking.this, "Loading list...", Toast.LENGTH_SHORT).show();

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                String name = "", hours = "", minutes = "";
                if(dataSnapshot.child("name").getValue() == null ||
                        dataSnapshot.child("hours").getValue() == null ||
                        dataSnapshot.child("minutes").getValue() == null){
                    return;
                }
                name = dataSnapshot.child("name").getValue().toString();
                hours = dataSnapshot.child("hours").getValue().toString();
                minutes = dataSnapshot.child("minutes").getValue().toString();
                UserGamePlaythrough playthrough = new UserGamePlaythrough(name,hours,minutes);
                listOfPlaythroughs.add(playthrough);
                playthroughAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        queryPlaythroughs = databaseReference.child("CurrentPlaythroughs").orderByChild("negativeTimestamp");
        queryPlaythroughs.addChildEventListener(childEventListener);



        listViewCurrentlyPlaying.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserGamePlaythrough selectedPlaythrough = (UserGamePlaythrough)playthroughAdapter.getItem(position);
                StartSessionTimerActivity(selectedPlaythrough.GetName());
            }
        });
    }

    private void StartSessionTimerActivity(String gameName) {
        Intent intent = new Intent(this, ActivitySessionTimer.class);
        intent.putExtra("GameName", gameName);
        queryPlaythroughs.removeEventListener(childEventListener);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        queryPlaythroughs.removeEventListener(childEventListener);
        super.onBackPressed();
    }
}
