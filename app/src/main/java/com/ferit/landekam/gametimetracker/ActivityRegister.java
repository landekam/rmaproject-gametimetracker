package com.ferit.landekam.gametimetracker;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ActivityRegister extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private EditText editTextEmail, editTextPassword, editTextPasswordRepeat;
    private Button buttonRegister;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        firebaseAuth = FirebaseAuth.getInstance();
        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        editTextEmail = findViewById(R.id.editTextEmailRegister);
        editTextPassword = findViewById(R.id.editTextPasswordRegister);
        editTextPasswordRepeat = findViewById(R.id.editTextPasswordRepeat);
        buttonRegister = findViewById(R.id.buttonRegister);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                if (email.isEmpty()) {
                    Toast.makeText(ActivityRegister.this, "Email field is empty.", Toast.LENGTH_SHORT).show();
                    return;
                }

                String password = editTextPassword.getText().toString().trim();
                String passwordRepeat = editTextPasswordRepeat.getText().toString().trim();
                if (password.isEmpty()) {
                    Toast.makeText(ActivityRegister.this, "Password field is empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.equals(passwordRepeat)) {
                    firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(ActivityRegister.this, "Successfully registered.", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                            else {
                                Toast.makeText(ActivityRegister.this, "Registration failed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else{
                        Toast.makeText(ActivityRegister.this, "Passwords don't match.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
