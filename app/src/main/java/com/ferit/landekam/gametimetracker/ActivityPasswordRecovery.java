package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ActivityPasswordRecovery extends AppCompatActivity {

    private EditText editTextEmail;
    private Button buttonSendEmail;
    private ImageButton imageButtonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);
        InitializeUI();
    }

    private void InitializeUI() {
        editTextEmail = findViewById(R.id.editTextEmail);
        buttonSendEmail = findViewById(R.id.buttonSendEmail);
        imageButtonBack = findViewById(R.id.imageButtonBack);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendPasswordRecoveryEmail();
            }
        });

    }

    private void SendPasswordRecoveryEmail() {
        String email = editTextEmail.getText().toString();

        FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(ActivityPasswordRecovery.this, "Email sent.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ActivityPasswordRecovery.this, "Could not send the email.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
