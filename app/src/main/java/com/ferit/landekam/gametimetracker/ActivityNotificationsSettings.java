package com.ferit.landekam.gametimetracker;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivityNotificationsSettings extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button buttonConfirm;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_settings);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid());

        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        radioGroup = findViewById(R.id.radioGroupSelectNotification);
        buttonConfirm = findViewById(R.id.buttonConfirm);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Confirm();
            }
        });
    }

    private void Confirm() {
        int radioButtonId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioButtonId);
        String radioButtonString = radioButton.getText().toString();


        if(radioButtonString.equals("Turn off")){
            databaseReference.child("millisecondsBetweenNotifications").setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ActivityNotificationsSettings.this,"Notifications turned off", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(ActivityNotificationsSettings.this,"Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else if(radioButtonString.equals("Every 30 minutes")){
            databaseReference.child("millisecondsBetweenNotifications").setValue(1800000).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ActivityNotificationsSettings.this,"Notifications set to every 30 minutes", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(ActivityNotificationsSettings.this,"Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else if(radioButtonString.equals("Every 60 minutes")){
            databaseReference.child("millisecondsBetweenNotifications").setValue(3600000).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ActivityNotificationsSettings.this,"Notifications set to every 60 minutes", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(ActivityNotificationsSettings.this,"Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else if(radioButtonString.equals("Every 90 minutes")){
            databaseReference.child("millisecondsBetweenNotifications").setValue(5400000).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ActivityNotificationsSettings.this,"Notifications set to every 90 minutes", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(ActivityNotificationsSettings.this,"Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else {
            databaseReference.child("millisecondsBetweenNotifications").setValue(7200000).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ActivityNotificationsSettings.this,"Notifications set to every 120 minutes", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(ActivityNotificationsSettings.this,"Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
