package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class PlaythroughAdapter extends BaseAdapter {

    private Activity Context;
    private ArrayList<UserGamePlaythrough> Playthroughs;
    private static LayoutInflater inflater = null;

    public PlaythroughAdapter(Activity context, ArrayList<UserGamePlaythrough> playthroughs){
        Context = context;
        Playthroughs = playthroughs;
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return Playthroughs.size();
    }

    @Override
    public Object getItem(int position) {
        return Playthroughs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = (itemView==null) ? inflater.inflate(R.layout.list_playthroughs_layout,null):itemView;
        TextView textViewGameName = itemView.findViewById(R.id.textViewGameName);
        TextView textViewTotalHoursAndMinutes = itemView.findViewById(R.id.textViewTotalHoursAndMinutes);
        UserGamePlaythrough selectedPlaythrough = Playthroughs.get(position);
        textViewGameName.setText(selectedPlaythrough.GetName());
        textViewTotalHoursAndMinutes.setText(selectedPlaythrough.GetTotalGametimeHoursAndMinutes());
        return itemView;
    }
}
