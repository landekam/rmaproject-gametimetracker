package com.ferit.landekam.gametimetracker;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ActivitySearchGames extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private EditText editTextSearch;
    private Button buttonSearch;
    private ListView listViewSearchResults;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ChildEventListener childEventListenerSearch;
    private Query querySearch;
    private ArrayList<Game> listOfSearchResultGames = new ArrayList<>();
    private GameAdapter gameAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_games);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        editTextSearch = findViewById(R.id.editTextSearch);
        buttonSearch = findViewById(R.id.buttonSearch);
        listViewSearchResults = findViewById(R.id.listViewSearchResults);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                querySearch.addChildEventListener(childEventListenerSearch);
                querySearch.removeEventListener(childEventListenerSearch);
                finish();
            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextSearch.getText().toString().equals("")){
                    Toast.makeText(ActivitySearchGames.this, "Search field is empty.", Toast.LENGTH_SHORT).show();
                }
                else {
                    SearchGames();
                }
            }
        });

        querySearch = databaseReference.child("games");

        childEventListenerSearch = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Toast toast = Toast.makeText(ActivitySearchGames.this,"Loading...",Toast.LENGTH_SHORT);
                toast.show();
                String id, name;
                id = dataSnapshot.getKey();
                name = dataSnapshot.child("name").getValue().toString();

                Game game = new Game(id,name);
                listOfSearchResultGames.add(game);
                gameAdapter.notifyDataSetChanged();
                toast.cancel();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        gameAdapter = new GameAdapter(this, listOfSearchResultGames);
        listViewSearchResults.setAdapter(gameAdapter);
        
        listViewSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Game selectedGame = (Game) gameAdapter.getItem(position);
                StartGameStatsActivity(selectedGame.GetId());
            }
        });
    }

    private void StartGameStatsActivity(String id) {
        Intent intent = new Intent(this, ActivityStatsGame.class);
        intent.putExtra("Id", id);
        startActivity(intent);
    }

    private void SearchGames() {
        Toast.makeText(ActivitySearchGames.this,"Searching...",Toast.LENGTH_LONG).show();
        querySearch.removeEventListener(childEventListenerSearch);
        listOfSearchResultGames.clear();
        gameAdapter.notifyDataSetChanged();
        String searchString = editTextSearch.getText().toString().toLowerCase().trim();
        Query search = querySearch.orderByChild("name").startAt(searchString).endAt(searchString+"\uf8ff").limitToFirst(10);
        search.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    Toast.makeText(ActivitySearchGames.this,"No results were found.",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        search.addChildEventListener(childEventListenerSearch);
    }

    @Override
    public void onBackPressed() {
        querySearch.addChildEventListener(childEventListenerSearch);
        querySearch.removeEventListener(childEventListenerSearch);
        super.onBackPressed();
    }
}
