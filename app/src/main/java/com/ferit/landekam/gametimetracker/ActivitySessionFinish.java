package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.UUID;

import static java.lang.Thread.sleep;


public class ActivitySessionFinish extends AppCompatActivity {

    private TextView textViewGameTitle, textViewPlaytimeHoursAndMinutes;
    private ImageButton imageButtonExit;
    private Button buttonMarkGameAsFinished;
    private String GameName;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ListView listViewSessions;
    private ArrayList<GameSession> listOfSessions = new ArrayList<>();
    private String selectedPlaystile = "Main game";
    private String GameId;
    private ChildEventListener childEventListener;
    private Query querySessions;
    private Toast toast;
    private String FinishedPlaythroughId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_finish);
        GameName = getIntent().getStringExtra("GameName");

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs");
        InitializeUI();
    }

    private void InitializeUI() {
        textViewGameTitle = findViewById(R.id.textViewGameTitle);
        textViewPlaytimeHoursAndMinutes = findViewById(R.id.textViewPlaytimeHoursAndMinutes);
        imageButtonExit = findViewById(R.id.imageButtonExit);
        buttonMarkGameAsFinished = findViewById(R.id.buttonMarkGameAsFinished);
        listViewSessions = findViewById(R.id.listViewSessions);

        textViewGameTitle.setText(GameName);


        final SessionAdapter sessionAdapter = new SessionAdapter(this, listOfSessions);
        listViewSessions.setAdapter(sessionAdapter);


        Query queryPlaythrough = databaseReference.orderByChild("name").equalTo(GameName);
        queryPlaythrough.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserGamePlaythrough playthrough = new UserGamePlaythrough(GameName);

                String playthroughHours = "0", playthroughMinutes = "0";
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    GameId = snapshot.getKey();
                    playthroughHours = snapshot.child("hours").getValue().toString();
                    playthroughMinutes = snapshot.child("minutes").getValue().toString();

                }

                playthrough.UpdateTotalGametime(playthroughHours, playthroughMinutes);
                textViewPlaytimeHoursAndMinutes.setText(playthrough.GetTotalGametimeHoursAndMinutes());

                childEventListener = new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        Toast.makeText(ActivitySessionFinish.this, "Loading list...", Toast.LENGTH_SHORT).show();
                        String hours = dataSnapshot.child("hours").getValue().toString();
                        String minutes = dataSnapshot.child("minutes").getValue().toString();
                        String date = dataSnapshot.child("date").getValue().toString();
                        GameSession gameSession = new GameSession(hours,minutes,date);

                        listOfSessions.add(gameSession);
                        sessionAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                };

                querySessions = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs").child(GameId).child("sessions").orderByChild("negativeTimestamp");
                querySessions.addChildEventListener(childEventListener);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        imageButtonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                querySessions.removeEventListener(childEventListener);
                finish();
            }
        });

        buttonMarkGameAsFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkGameAsFinishedDialog();
            }
        });

    }

    private void MarkGameAsFinishedDialog() {
        final String[] playstyles = this.getResources().getStringArray(R.array.playstiles);
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Please select your playstyle.");
        dialogBuilder.setSingleChoiceItems(R.array.playstiles, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedPlaystile = playstyles[which];
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StoreInDatabase();
            }
        });

        dialogBuilder.show();
    }

    private void StoreInDatabase()  {
        toast = Toast.makeText(ActivitySessionFinish.this, "Loading...", Toast.LENGTH_LONG);
        toast.show();
        String[] hoursAndMinutes = textViewPlaytimeHoursAndMinutes.getText().toString().split(":");
        DatabaseReference CompletedPlaythroughsReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("FinishedPlaythroughs");
        String gameId = UUID.randomUUID().toString();

        FinishedPlaythroughId = gameId;
        CompletedPlaythroughsReference.child(gameId).child("nameFinished").setValue(GameName);
        CompletedPlaythroughsReference.child(gameId).child("hours").setValue(hoursAndMinutes[0]);
        CompletedPlaythroughsReference.child(gameId).child("minutes").setValue(hoursAndMinutes[1]);
        CompletedPlaythroughsReference.child(gameId).child("playstyle").setValue(selectedPlaystile).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                StoreFinishedPlaythroughIntoGames();
            }
        });


    }

    private void StoreFinishedPlaythroughIntoGames() {
        final DatabaseReference GamesPlaythroughsReference = firebaseDatabase.getReference().child("games");
        Query query = GamesPlaythroughsReference.orderByChild("name").equalTo(GameName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String gameId = "";
                for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    gameId = snapshot.getKey();
                }
                String playthroughId = UUID.randomUUID().toString();
                String[] hoursAndMinutes = textViewPlaytimeHoursAndMinutes.getText().toString().split(":");
                GamesPlaythroughsReference.child(gameId).child("playthroughs").child(playthroughId).child("hours").setValue(hoursAndMinutes[0]);
                GamesPlaythroughsReference.child(gameId).child("playthroughs").child(playthroughId).child("minutes").setValue(hoursAndMinutes[1]);
                GamesPlaythroughsReference.child(gameId).child("playthroughs").child(playthroughId).child("playstyle").setValue(selectedPlaystile).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        DeleteFromDatabase();
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void DeleteFromDatabase() {
        DatabaseReference deleteReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs").child(GameId);
        deleteReference.setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                toast.cancel();
                querySessions.removeEventListener(childEventListener);
                StartStatsFinishedPlaythroughActivity();
            }
        });
    }

    private void StartStatsFinishedPlaythroughActivity() {
        Intent intent = new Intent(this, ActivityStatsFinishedPlaythrough.class);
        intent.putExtra("Id", FinishedPlaythroughId);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        querySessions.removeEventListener(childEventListener);
        super.onBackPressed();
    }
}
