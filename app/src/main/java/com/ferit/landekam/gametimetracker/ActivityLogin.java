package com.ferit.landekam.gametimetracker;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ActivityLogin extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private EditText editTextEmail, editTextPassword;
    private Button buttonLogIn, buttonRegister, buttonRecoverPassword;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        firebaseAuth = FirebaseAuth.getInstance();
        InitializeUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null){
            onBackPressed();
        }
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogIn = findViewById(R.id.buttonLogIn);
        buttonRegister = findViewById(R.id.buttonRegistration);
        buttonRecoverPassword = findViewById(R.id.buttonRecoverPassword);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartRegisterActivity();
            }
        });

        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();
                if (email.isEmpty()) {
                    Toast.makeText(ActivityLogin.this, "Email field is empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.isEmpty()) {
                    Toast.makeText(ActivityLogin.this, "Password field is empty.", Toast.LENGTH_SHORT).show();
                    return;
                }
                firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(ActivityLogin.this, "Logged in successfully.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else {
                            Toast.makeText(ActivityLogin.this, "Authentication failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        buttonRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartPasswordRecoveryActivity();
            }
        });
    }

    private void StartPasswordRecoveryActivity() {
        Intent intent = new Intent(this, ActivityPasswordRecovery.class);
        startActivity(intent);
    }

    private void StartRegisterActivity() {
        Intent intent = new Intent(this, ActivityRegister.class);
        startActivity(intent);
    }


}
