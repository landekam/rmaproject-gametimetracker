package com.ferit.landekam.gametimetracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ActivityMain extends AppCompatActivity {

    private ImageButton imageButtonExit, imageButtonUserLogin;
    private Button buttonStartTracking, buttonMyLibrary,
            buttonSearchGames, buttonNotificationsSettings;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        if(firebaseUser != null){
            Toast.makeText(ActivityMain.this,  firebaseUser.getEmail() + " logged in.", Toast.LENGTH_SHORT).show();
        }
        InitializeUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseUser = firebaseAuth.getCurrentUser();
    }

    private void InitializeUI() {
        imageButtonExit = findViewById(R.id.imageButtonExit);
        imageButtonUserLogin = findViewById(R.id.imageButtonUserLogin);
        buttonStartTracking = findViewById(R.id.buttonStartTracking);
        buttonMyLibrary = findViewById(R.id.buttonMyLibrary);
        buttonSearchGames = findViewById(R.id.buttonSearchGames);
        buttonNotificationsSettings = findViewById(R.id.buttonNotificationsSettings);

        imageButtonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExitDialog();
            }
        });

        imageButtonUserLogin.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firebaseUser == null) {
                    StartUserLoginActivity();
                }
                else {
                    StartUserLogoutActivity();
                }
            }
        }));

        buttonStartTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firebaseUser == null) {
                    Toast.makeText(ActivityMain.this, "Please log in.", Toast.LENGTH_SHORT).show();
                }
                else {
                    StartStartTrackingActivity();
                }
            }
        });

        buttonMyLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firebaseUser == null) {
                    Toast.makeText(ActivityMain.this, "Please log in.", Toast.LENGTH_SHORT).show();
                }
                else {
                    StartMyLibraryActivity();
                }
            }
        });

        buttonSearchGames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firebaseUser == null) {
                    Toast.makeText(ActivityMain.this, "Please log in.", Toast.LENGTH_SHORT).show();
                }
                else {
                    StartSearchGamesActivity();
                }
            }
        });

        buttonNotificationsSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firebaseUser == null) {
                    Toast.makeText(ActivityMain.this, "Please log in.", Toast.LENGTH_SHORT).show();
                }
                else {
                    StartAlarmAndNotificationsActivity();
                }
            }
        });
    }

    private void StartAlarmAndNotificationsActivity() {
        Intent intent = new Intent(this, ActivityNotificationsSettings.class);
        startActivity(intent);
    }

    private void StartSearchGamesActivity() {
        Intent intent = new Intent(this, ActivitySearchGames.class);
        startActivity(intent);
    }

    private void StartMyLibraryActivity() {
        Intent intent = new Intent(this, ActivityLibrary.class);
        startActivity(intent);
    }

    private void StartStartTrackingActivity() {
        Intent intent = new Intent(this, ActivityStartTracking.class);
        startActivity(intent);
    }

    private void StartUserLogoutActivity() {
        Intent intent = new Intent(this, ActivityLogout.class);
        startActivity(intent);
    }

    private void StartUserLoginActivity() {
        Intent intent = new Intent(this, ActivityLogin.class);
        startActivity(intent);
    }

    private void ExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure you want to exit?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        ExitDialog();
        return;
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseUser = firebaseAuth.getCurrentUser();
    }
}
