package com.ferit.landekam.gametimetracker;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class UserGamePlaythrough {
    private String Id;
    private String Name;
    private String TotalGametimeHours;
    private String TotalGametimeMinutes;

    public UserGamePlaythrough(){

    }

    public UserGamePlaythrough(String name){
        Name = name;
        TotalGametimeHours = "0";
        TotalGametimeMinutes = "0";
    }

    public UserGamePlaythrough(String name, String hours, String minutes){
        Name = name;
        TotalGametimeHours = hours;
        TotalGametimeMinutes = minutes;
    }

    public String GetTotalGametimeHoursAndMinutes(){
        String gametimeHoursString, gametimeMintuesString;
        if (Integer.valueOf(TotalGametimeMinutes) < 10){
            gametimeMintuesString = "0" + TotalGametimeMinutes;
        }
        else {
            gametimeMintuesString = TotalGametimeMinutes;
        }
        if (Integer.valueOf(TotalGametimeHours) < 10){
            gametimeHoursString = "0" + TotalGametimeHours;
        }
        else {
            gametimeHoursString = TotalGametimeHours;
        }
        String totalGametimeString = gametimeHoursString + ":" + gametimeMintuesString;
        return totalGametimeString;
    }

    public void UpdateTotalGametime(String hour, String minutes){
        int hoursInt = Integer.valueOf(hour) + Integer.valueOf(TotalGametimeHours);
        int minutesInt = Integer.valueOf(minutes) + Integer.valueOf(TotalGametimeMinutes);
        hoursInt += minutesInt/60;
        minutesInt = minutesInt % 60;
        TotalGametimeMinutes = String.valueOf(minutesInt);
        TotalGametimeHours = String.valueOf(hoursInt);
    }

    public String GetName(){
        return Name;
    }

    public String GetTotalGametimeHours(){
        return  TotalGametimeHours;
    }

    public String GetTotalGametimeMinutes(){
        return  TotalGametimeMinutes;
    }

    public void SetId(String id){
        Id = id;
    }

    public String GetId(){
       return Id;
    }
}
