package com.ferit.landekam.gametimetracker;

public class Game {
    private String Id;
    private String Name;

    public Game(String id, String name){
        Id = id;
        Name = name;
    }

    public String GetId() {
        return Id;
    }

    public String GetName() {
        return Name;
    }
}
