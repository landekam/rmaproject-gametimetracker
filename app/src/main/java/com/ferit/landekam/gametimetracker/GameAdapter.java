package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class GameAdapter extends BaseAdapter {

    private Activity Context;
    private ArrayList<Game> Games;
    private static LayoutInflater inflater = null;

    public GameAdapter(Activity context, ArrayList<Game> games){
        Context = context;
        Games = games;
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Games.size();
    }

    @Override
    public Object getItem(int position) {
        return Games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = (itemView==null) ? inflater.inflate(R.layout.list_games_layout,null):itemView;
        TextView textViewGameName = itemView.findViewById(R.id.textViewGameName);
        Game selectedGame = Games.get(position);
        textViewGameName.setText(selectedGame.GetName());
        return itemView;
    }
}
