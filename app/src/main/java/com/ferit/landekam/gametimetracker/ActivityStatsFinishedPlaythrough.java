package com.ferit.landekam.gametimetracker;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ActivityStatsFinishedPlaythrough extends AppCompatActivity {

    private TextView textViewGameTitle, textViewPlaytimeHoursAndMinutes, textViewPlaystyleType;
    private ImageButton imageButtonBack;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private String gameId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_finished_playthrough);
        gameId = getIntent().getStringExtra("Id");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("FinishedPlaythroughs");
        InitializeUI();
    }

    private void InitializeUI() {
        textViewGameTitle = findViewById(R.id.textViewGameTitle);
        textViewPlaytimeHoursAndMinutes = findViewById(R.id.textViewPlaytimeHoursAndMinutes);
        textViewPlaystyleType =findViewById(R.id.textViewPlaystyleType);
        imageButtonBack = findViewById(R.id.imageButtonBack);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Toast.makeText(ActivityStatsFinishedPlaythrough.this, "Loading...", Toast.LENGTH_LONG).show();
        Query query = databaseReference.child(gameId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String gameName, playtimeMinutes,playtimeHours, playstyle;
                gameName = dataSnapshot.child("nameFinished").getValue().toString();
                playtimeHours = dataSnapshot.child("hours").getValue().toString();
                playtimeMinutes = dataSnapshot.child("minutes").getValue().toString();
                playstyle = dataSnapshot.child("playstyle").getValue().toString();

                textViewGameTitle.setText(gameName);
                textViewPlaytimeHoursAndMinutes.setText(playtimeHours + ":" + playtimeMinutes);
                textViewPlaystyleType.setText(playstyle);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
