package com.ferit.landekam.gametimetracker;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

public class ActivityLibrary extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private Button buttonChangeDisplay;
    private ListView listViewGames;
    private TextView textViewCurrentFinished;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ChildEventListener childEventListenerCurrent, childEventListenerFinished;
    private ArrayList<UserGamePlaythrough> listOfCurrentPlaythroughs = new ArrayList<>(), listOfFinishedPlaythroughs = new ArrayList<>();
    private Query queryCurrentPlaythroughs, queryFinishedPlaythroughs;
    private PlaythroughAdapter currentPlaythroughAdapter, finishedPlaythroughAdapter;
    private Boolean IsSetToCurrent = true;
    private AdapterView.OnItemClickListener currentPlaythroughListener, finishedPlaythroughListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid());
        InitializeUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        InitializeLists();
    }

    private void InitializeLists() {
        Toast.makeText(ActivityLibrary.this, "Loading...", Toast.LENGTH_SHORT).show();
        listOfCurrentPlaythroughs.clear();
        listOfFinishedPlaythroughs.clear();
        currentPlaythroughAdapter.notifyDataSetChanged();
        finishedPlaythroughAdapter.notifyDataSetChanged();


        queryCurrentPlaythroughs = databaseReference.child("CurrentPlaythroughs").orderByChild("name");
        queryCurrentPlaythroughs.addChildEventListener(childEventListenerCurrent);

        queryFinishedPlaythroughs = databaseReference.child("FinishedPlaythroughs").orderByChild("nameFinished");
        queryFinishedPlaythroughs.addChildEventListener(childEventListenerFinished);
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        listViewGames = findViewById(R.id.listViewGames);
        buttonChangeDisplay = findViewById(R.id.buttonChangeDisplay);
        textViewCurrentFinished = findViewById(R.id.textViewCurrentFinished);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                queryCurrentPlaythroughs.removeEventListener(childEventListenerCurrent);
                queryFinishedPlaythroughs.removeEventListener(childEventListenerFinished);
                finish();
            }
        });

        buttonChangeDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeList();
            }
        });

        currentPlaythroughAdapter = new PlaythroughAdapter(this, listOfCurrentPlaythroughs);
        finishedPlaythroughAdapter = new PlaythroughAdapter(this, listOfFinishedPlaythroughs);
        listViewGames.setAdapter(currentPlaythroughAdapter);

        childEventListenerCurrent = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Toast.makeText(ActivityLibrary.this, "Loading...", Toast.LENGTH_SHORT).show();
                String name = "", hours = "", minutes = "", id="";
                id = dataSnapshot.getKey();
                name = dataSnapshot.child("name").getValue().toString();
                hours = dataSnapshot.child("hours").getValue().toString();
                minutes = dataSnapshot.child("minutes").getValue().toString();
                UserGamePlaythrough playthrough = new UserGamePlaythrough(name,hours,minutes);
                playthrough.SetId(id);
                listOfCurrentPlaythroughs.add(playthrough);
                currentPlaythroughAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        childEventListenerFinished = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Toast.makeText(ActivityLibrary.this, "Loading...", Toast.LENGTH_SHORT).show();
                String name = "", hours = "", minutes = "", id = "";
                if(dataSnapshot.child("nameFinished").getValue() == null ||
                        dataSnapshot.child("hours").getValue() == null ||
                dataSnapshot.child("minutes").getValue() == null){
                    return;
                }
                id = dataSnapshot.getKey();
                name = dataSnapshot.child("nameFinished").getValue().toString();
                hours = dataSnapshot.child("hours").getValue().toString();
                if(hours.charAt(0) == '0'){
                    hours = hours.substring(1);
                }
                minutes = dataSnapshot.child("minutes").getValue().toString();
                if(minutes.charAt(0) == '0'){
                    minutes = minutes.substring(1);
                }
                UserGamePlaythrough playthrough = new UserGamePlaythrough(name,hours,minutes);
                playthrough.SetId(id);
                listOfFinishedPlaythroughs.add(playthrough);
                finishedPlaythroughAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        currentPlaythroughListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserGamePlaythrough selectedPlaythrough = (UserGamePlaythrough)currentPlaythroughAdapter.getItem(position);
                StartCurrentPlaythroughStatsActivity(selectedPlaythrough.GetId());
            }
        };

        finishedPlaythroughListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserGamePlaythrough selectedPlaythrough = (UserGamePlaythrough)finishedPlaythroughAdapter.getItem(position);
                StartFinishedPlaythroughStatsActivity(selectedPlaythrough.GetId());
            }
        };

        listViewGames.setOnItemClickListener(currentPlaythroughListener);
    }

    private void ChangeList() {
        if(IsSetToCurrent) {
            IsSetToCurrent = false;
            listViewGames.setAdapter(finishedPlaythroughAdapter);
            listViewGames.setOnItemClickListener(finishedPlaythroughListener);
            buttonChangeDisplay.setText("Display current");
            textViewCurrentFinished.setText("Finished playthroughs:");
        }
        else {
            IsSetToCurrent = true;
            listViewGames.setAdapter(currentPlaythroughAdapter);
            listViewGames.setOnItemClickListener(currentPlaythroughListener);
            buttonChangeDisplay.setText("Display finished");
            textViewCurrentFinished.setText("Current playthroughs:");
        }
    }

    private void StartCurrentPlaythroughStatsActivity(String id){
        queryCurrentPlaythroughs.removeEventListener(childEventListenerCurrent);
        queryFinishedPlaythroughs.removeEventListener(childEventListenerFinished);
        Intent intent = new Intent(this, ActivityStatsCurrentPlaythrough.class);
        intent.putExtra("Id", id);
        startActivity(intent);
    }

    private void StartFinishedPlaythroughStatsActivity(String id){
        queryCurrentPlaythroughs.removeEventListener(childEventListenerCurrent);
        queryFinishedPlaythroughs.removeEventListener(childEventListenerFinished);
        Intent intent = new Intent(this, ActivityStatsFinishedPlaythrough.class);
        intent.putExtra("Id", id);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        queryCurrentPlaythroughs.removeEventListener(childEventListenerCurrent);
        queryFinishedPlaythroughs.removeEventListener(childEventListenerFinished);
        super.onBackPressed();
    }
}
