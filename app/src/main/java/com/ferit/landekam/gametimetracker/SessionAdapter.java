package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class SessionAdapter extends BaseAdapter {
    private Activity Context;
    private ArrayList<GameSession> Sessions;
    private static LayoutInflater inflater = null;

    public SessionAdapter(Activity context, ArrayList<GameSession> sessions){
        Context = context;
        Sessions = sessions;
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Sessions.size();
    }

    @Override
    public Object getItem(int position) {
        return Sessions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = (itemView==null) ? inflater.inflate(R.layout.list_sessions_layout,null):itemView;
        TextView textViewHoursAndMinutes = itemView.findViewById(R.id.textViewHoursAndMinutes);
        TextView textViewDate = itemView.findViewById(R.id.textViewDate);
        GameSession selectedSession = Sessions.get(position);
        textViewHoursAndMinutes.setText(selectedSession.GetSessionGametimeHoursAndMinutes());
        textViewDate.setText(selectedSession.GetDate());
        return itemView;
    }
}
