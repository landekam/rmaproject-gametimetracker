package com.ferit.landekam.gametimetracker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class ActivityAddNewGame extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private Button buttonAdd, buttonSearch;
    private EditText editTextGameName;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, databaseGamesReference;
    private Toast toast;
    private ChildEventListener childEventListenerSearch;
    private Query querySearch;
    private ArrayList<Game> listOfSearchResultGames = new ArrayList<>();
    private GameAdapter gameAdapter;
    private ListView listViewSearchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_game);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs");
        databaseGamesReference = firebaseDatabase.getReference();
        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        buttonAdd = findViewById(R.id.buttonAdd);
        editTextGameName = findViewById(R.id.editTextGameName);
        buttonSearch = findViewById(R.id.buttonSearch);
        listViewSearchResults = findViewById(R.id.listViewSearchResults);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartStartTrackingActivity();
                onBackPressed();
            }
        });

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextGameName.getText().toString().equals("")){
                    Toast.makeText(ActivityAddNewGame.this, "Search field is empty.", Toast.LENGTH_SHORT).show();
                }
                else {
                    SearchGames();
                }
            }
        });

        querySearch = databaseGamesReference.child("games");

        childEventListenerSearch = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Toast toast = Toast.makeText(ActivityAddNewGame.this,"Loading...",Toast.LENGTH_SHORT);
                toast.show();
                String id, name;
                id = dataSnapshot.getKey();
                name = dataSnapshot.child("name").getValue().toString();

                Game game = new Game(id,name);
                listOfSearchResultGames.add(game);
                gameAdapter.notifyDataSetChanged();
                toast.cancel();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        gameAdapter = new GameAdapter(this, listOfSearchResultGames);
        listViewSearchResults.setAdapter(gameAdapter);

        listViewSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Game selectedGame = (Game) gameAdapter.getItem(position);
                editTextGameName.setText(selectedGame.GetName());
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String gameName = editTextGameName.getText().toString().toLowerCase().trim();
                if(gameName.equals("")){
                    Toast.makeText(ActivityAddNewGame.this, "Please enter the name of the game.", Toast.LENGTH_SHORT).show();
                    return;
                }
                toast = Toast.makeText(ActivityAddNewGame.this,"Loading...", Toast.LENGTH_LONG);
                toast.show();

                Query query = databaseGamesReference.orderByChild("name").equalTo(gameName);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            Toast.makeText(ActivityAddNewGame.this, "Already exists.", Toast.LENGTH_SHORT).show();
                            StartSessionTimerActivity(gameName);
                        }
                        else {

                            AddNewGameToDatabase();

                            UserGamePlaythrough newPlaythrough = new UserGamePlaythrough(gameName);
                            String gameId = UUID.randomUUID().toString();

                            databaseReference.child(gameId).child("negativeTimestamp").setValue(-System.currentTimeMillis());
                            databaseReference.child(gameId).child("name").setValue(newPlaythrough.GetName());
                            databaseReference.child(gameId).child("hours").setValue(newPlaythrough.GetTotalGametimeHours());
                            databaseReference.child(gameId).child("minutes").setValue(newPlaythrough.GetTotalGametimeMinutes()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    toast.cancel();
                                    StartSessionTimerActivity(gameName);
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    private void SearchGames() {
        Toast.makeText(ActivityAddNewGame.this,"Searching...",Toast.LENGTH_LONG).show();
        querySearch.removeEventListener(childEventListenerSearch);
        listOfSearchResultGames.clear();
        gameAdapter.notifyDataSetChanged();
        String searchString = editTextGameName.getText().toString().toLowerCase().trim();
        Query search = querySearch.orderByChild("name").startAt(searchString).endAt(searchString+"\uf8ff").limitToFirst(10);
        search.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    Toast.makeText(ActivityAddNewGame.this,"No results were found.",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        search.addChildEventListener(childEventListenerSearch);
    }

    private void AddNewGameToDatabase() {
        final DatabaseReference gamesReference = firebaseDatabase.getReference().child("games");
        final String name = editTextGameName.getText().toString().toLowerCase().trim();
        Query query = gamesReference.orderByChild("name").equalTo(name);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                }
                else{
                    String gameId = UUID.randomUUID().toString();

                    gamesReference.child(gameId).child("name").setValue(name);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void StartStartTrackingActivity() {
        querySearch.addChildEventListener(childEventListenerSearch);
        querySearch.removeEventListener(childEventListenerSearch);
        Intent intent = new Intent(this, ActivityStartTracking.class);
        startActivity(intent);
        finish();
    }

    private void StartSessionTimerActivity(String GameName) {
        querySearch.addChildEventListener(childEventListenerSearch);
        querySearch.removeEventListener(childEventListenerSearch);
        Intent intent = new Intent(this, ActivitySessionTimer.class);
        intent.putExtra("GameName", GameName);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        querySearch.addChildEventListener(childEventListenerSearch);
        querySearch.removeEventListener(childEventListenerSearch);
        super.onBackPressed();
    }
}
