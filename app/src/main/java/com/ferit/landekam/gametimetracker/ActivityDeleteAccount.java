package com.ferit.landekam.gametimetracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ActivityDeleteAccount extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private EditText editTextEmail, editTextPassword;
    private Button buttonDeleteAccount;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonDeleteAccount = findViewById(R.id.buttonDeleteAccount);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextEmail.getText().toString().equals("") || editTextPassword.getText().toString().equals("")) {
                    Toast.makeText(ActivityDeleteAccount.this, "Email or password field empty.", Toast.LENGTH_SHORT).show();
                }
                else {
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(ActivityDeleteAccount.this);
                    deleteDialog.setTitle("Delete playthrough");
                    deleteDialog.setMessage("Are you sure you want do delete?");
                    deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DeleteAcconut();
                        }
                    });
                    deleteDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    deleteDialog.show();
                }


            }
        });
    }

    private void DeleteAcconut() {
        String email, password;
        email = editTextEmail.getText().toString();
        password = editTextPassword.getText().toString();

        AuthCredential credential = EmailAuthProvider.getCredential(email,password);
        firebaseUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    firebaseUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ActivityDeleteAccount.this, "Successfully deleted account.", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(ActivityDeleteAccount.this, "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(ActivityDeleteAccount.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
