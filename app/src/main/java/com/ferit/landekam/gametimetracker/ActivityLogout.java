package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ActivityLogout extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private Button buttonLogOut, buttonDeleteAccount;
    private TextView textViewUserEmail;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        firebaseAuth = FirebaseAuth.getInstance();
        InitializeUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (FirebaseAuth.getInstance() == null){
            finish();
        }
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        buttonLogOut = findViewById(R.id.buttonLogOut);
        textViewUserEmail = findViewById(R.id.textViewUserEmail);
        buttonDeleteAccount = findViewById(R.id.buttonDeleteAccount);

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        firebaseUser = firebaseAuth.getCurrentUser();
        String email = firebaseUser.getEmail();
        textViewUserEmail.setText(email);

        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                Toast.makeText(ActivityLogout.this, "Logged out.", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });

        buttonDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartDeleteAccountActivity();
            }
        });
    }

    private void StartDeleteAccountActivity() {
        Intent intent = new Intent(this, ActivityDeleteAccount.class);
        startActivity(intent);
    }
}
