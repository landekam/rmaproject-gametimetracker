package com.ferit.landekam.gametimetracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


public class ActivitySessionTimer extends AppCompatActivity {

    private String GameName;
    private Button buttonStartPause, buttonFinishSession;
    private Chronometer chronometer;
    private boolean paused = true;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private long millisecondsElapsed = 0;
    private long startTimeMilliseconds = 0, endTimeMilliseconds = 0;
    private long pauseOffset;
    private Toast toast;
    private NotificationManagerCompat notificationManager;
    private long millisToNotification;
    private  Handler handler = new Handler();
    private  boolean notificationsSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameName = getIntent().getStringExtra("GameName");
        setContentView(R.layout.activity_session_timer);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs");
        notificationManager = NotificationManagerCompat.from(this);
        SetNumberOfMillisecondsBetweenNotifications();

    }

    private void SetNumberOfMillisecondsBetweenNotifications() {
        Query query = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("millisecondsBetweenNotifications");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    millisToNotification = Long.parseLong(dataSnapshot.getValue().toString());
                    CreateChannel();
                    notificationsSet = true;
                }
                InitializeUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });

    }

    private void InitializeUI() {
        buttonStartPause = findViewById(R.id.buttonStartPause);
        buttonFinishSession = findViewById(R.id.buttonFinishSession);
        chronometer = findViewById(R.id.chronometer);
        chronometer.setText("00:00:00");
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int hours = (int)time / 3600000;
                int minutes = ((int)time - hours*3600000)/60000;
                int seconds = ((int)time - hours*3600000 - minutes*60000)/1000 ;
                String hh = hours < 10 ? "0"+hours: hours+"";
                String mm = minutes < 10 ? "0"+minutes: minutes+"";
                String ss = seconds < 10 ? "0"+seconds: seconds+"";
                chronometer.setText(hh+":"+mm+":"+ss);
                }
            }
        );

        buttonFinishSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FinishDialog();
            }
        });

        buttonStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartPause();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        FileOutputStream fileOutputStream = null;

            try {
                fileOutputStream = openFileOutput("paused.txt", MODE_PRIVATE);
                if(paused){
                    fileOutputStream.write("1".getBytes());
                }
                else {
                    fileOutputStream.write("0".getBytes());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if(fileOutputStream != null){
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                fileOutputStream = openFileOutput("elapsed.txt", MODE_PRIVATE);
                fileOutputStream.write(Long.toString(millisecondsElapsed).getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if(fileOutputStream != null){
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                fileOutputStream = openFileOutput("millis.txt", MODE_PRIVATE);
                fileOutputStream.write(Long.toString(System.currentTimeMillis()).getBytes());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if(fileOutputStream != null){
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


    }

    @Override
    protected void onStart() {
        super.onStart();


        FileInputStream fileInputStream = null;
        String paused = null, elapsed = null, millis = null;


        File file = new File("paused.txt");
        if (file.exists()) {
            try {
                fileInputStream = openFileInput("paused.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                while ((paused = bufferedReader.readLine()) != null) {

                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                fileInputStream = openFileInput("elapsed.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                while ((elapsed = bufferedReader.readLine()) != null) {

                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                fileInputStream = openFileInput("millis.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                while ((millis = bufferedReader.readLine()) != null) {

                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            long time = 0;
            if (paused.equals("1")) {
                time = Long.parseLong(elapsed);
            } else {
                time = Long.parseLong(elapsed) + (System.currentTimeMillis() - Long.parseLong(millis));
            }

            chronometer.setBase(SystemClock.elapsedRealtime() - time);
        }
    }

    private void StartPause() {
        if(paused){
            startTimeMilliseconds = System.currentTimeMillis();
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            paused = false;
            buttonStartPause.setText("Pause");
            if(notificationsSet) {
                handler.postDelayed(notificationDelay, millisToNotification - (millisecondsElapsed % millisToNotification));
            }
        }
        else {
            endTimeMilliseconds = System.currentTimeMillis();
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            paused = true;
            buttonStartPause.setText("Resume");
            millisecondsElapsed += (endTimeMilliseconds - startTimeMilliseconds);
            if(notificationsSet) {
                handler.removeCallbacks(notificationDelay);
            }
        }
    }

    private void FinishDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Finish");
        builder.setMessage("Are you sure you want to finish this session?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FinishSession();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        FinishDialog();
    }

    private void FinishSession() {
        if(notificationsSet) {
            handler.removeCallbacks(notificationDelay);
        }
        toast = Toast.makeText(ActivitySessionTimer.this, "Loading...", Toast.LENGTH_LONG);
        toast.show();
        if (!paused){
            paused = true;
            chronometer.stop();
            endTimeMilliseconds = System.currentTimeMillis();
            millisecondsElapsed += (endTimeMilliseconds - startTimeMilliseconds);
        }
        final String minutes = String.valueOf((millisecondsElapsed / 60000)%60);
        final String hours = String.valueOf(millisecondsElapsed / 3600000);
        SimpleDateFormat formatter= new SimpleDateFormat("dd.MM.yyyy.");
        final Date date = new Date(System.currentTimeMillis());
        final String dateString = formatter.format(date);

        Query query = databaseReference.orderByChild("name").equalTo(GameName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserGamePlaythrough playthrough = new UserGamePlaythrough(GameName);

                String playthroughHours = "0", playthroughMinutes = "0", gameId = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    gameId = snapshot.getKey();
                    playthroughHours = snapshot.child("hours").getValue().toString();
                    playthroughMinutes = snapshot.child("minutes").getValue().toString();
                }
                playthrough.UpdateTotalGametime(playthroughHours,playthroughMinutes);
                playthrough.UpdateTotalGametime(hours,minutes);
                databaseReference.child(gameId).child("name").setValue(playthrough.GetName());
                databaseReference.child(gameId).child("hours").setValue(playthrough.GetTotalGametimeHours());
                databaseReference.child(gameId).child("minutes").setValue(playthrough.GetTotalGametimeMinutes());
                databaseReference.child(gameId).child("negativeTimestamp").setValue(-System.currentTimeMillis());

                String sessionId = UUID.randomUUID().toString();
                databaseReference = databaseReference.child(gameId).child("sessions").child(sessionId);
                databaseReference.child("hours").setValue(hours);
                databaseReference.child("minutes").setValue(minutes);
                databaseReference.child("date").setValue(dateString);
                databaseReference.child("negativeTimestamp").setValue(-System.currentTimeMillis()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        toast.cancel();
                        StartSessionFinishActivity();
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void StartSessionFinishActivity(){
        final Intent intent = new Intent(this,ActivitySessionFinish.class);
        intent.putExtra("GameName", GameName);
        startActivity(intent);
        finish();
    }

    private void ShowNotification(){
        long time = SystemClock.elapsedRealtime() - chronometer.getBase();
        int hours = (int)time / 3600000;
        int minutes = ((int)time - hours*3600000)/60000;
        int seconds = ((int)time - hours*3600000 - minutes*60000)/1000 ;
        String hh = hours < 10 ? "0"+hours: hours+"";
        String mm = minutes < 10 ? "0"+minutes: minutes+"";
        String ss = seconds < 10 ? "0"+seconds: seconds+"";
        String displayTime = hh + ":" + mm + ":" + ss;

        Notification notification = new NotificationCompat.Builder(this, "Channel1")
                .setSmallIcon(R.drawable.notif).setContentTitle("Gametime")
                .setContentText("You have been playing for: " + displayTime)
                .build();

        notificationManager.notify(1, notification);
    }

    private void CreateChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel1 = new NotificationChannel(
                    "Channel1",
                    "Time playing notifications",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel1.setDescription("This provides info how long have you played in set time intervals.");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
        }
    }


    private Runnable notificationDelay = new Runnable() {
        @Override
        public void run() {
            ShowNotification();
            handler.postDelayed(this, millisToNotification - ((SystemClock.elapsedRealtime() - chronometer.getBase()) % millisToNotification));
        }
    };
}
