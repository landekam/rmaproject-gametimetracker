package com.ferit.landekam.gametimetracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.UUID;

public class ActivityStatsCurrentPlaythrough extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private TextView textViewGameTitle, textViewPlaytimeHoursAndMinutes;
    private Button buttonMarkGameAsFinished,buttonResetProgress,buttonDeletePlaythrough;
    private ListView listViewSessions;
    private String gameId;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ChildEventListener childEventListener;
    private ArrayList<GameSession> listOfSessions = new ArrayList<>();
    private String selectedPlaystile = "Main game";
    private Toast toast;
    private Query querySessions;
    private SessionAdapter sessionAdapter;
    private String finishedGameId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_current_playthrough);
        gameId = getIntent().getStringExtra("Id");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs");
        InitializeUI();
    }

    private void InitializeUI() {
        imageButtonBack = findViewById(R.id.imageButtonBack);
        textViewGameTitle = findViewById(R.id.textViewGameTitle);
        textViewPlaytimeHoursAndMinutes = findViewById(R.id.textViewPlaytimeHoursAndMinutes);
        buttonMarkGameAsFinished = findViewById(R.id.buttonMarkGameAsFinished);
        buttonResetProgress = findViewById(R.id.buttonResetProgress);
        buttonDeletePlaythrough = findViewById(R.id.buttonDeletePlaythrough);
        listViewSessions = findViewById(R.id.listViewSessions);

        sessionAdapter = new SessionAdapter(this, listOfSessions);
        listViewSessions.setAdapter(sessionAdapter);
        querySessions = databaseReference.child(gameId).child("sessions").orderByChild("negativeTimestamp");

        Query queryGame = databaseReference.child(gameId);
        queryGame.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String gameName, playtimeMinutes,playtimeHours;
                gameName = dataSnapshot.child("name").getValue().toString();
                playtimeHours = dataSnapshot.child("hours").getValue().toString();
                playtimeMinutes = dataSnapshot.child("minutes").getValue().toString();

                UserGamePlaythrough userGamePlaythrough = new UserGamePlaythrough(gameName,playtimeHours,playtimeMinutes);

                textViewGameTitle.setText(userGamePlaythrough.GetName());
                textViewPlaytimeHoursAndMinutes.setText(userGamePlaythrough.GetTotalGametimeHoursAndMinutes());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                querySessions.removeEventListener(childEventListener);
                finish();
            }
        });

        buttonMarkGameAsFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MarkGameAsFinished();
            }
        });

        buttonResetProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetProgress();
            }
        });

        buttonDeletePlaythrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeletePlaythrough();
            }
        });

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String hours = dataSnapshot.child("hours").getValue().toString();
                String minutes = dataSnapshot.child("minutes").getValue().toString();
                String date = dataSnapshot.child("date").getValue().toString();
                GameSession gameSession = new GameSession(hours,minutes,date);

                listOfSessions.add(gameSession);
                sessionAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        querySessions.addChildEventListener(childEventListener);

    }

    private void DeletePlaythrough() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle("Delete playthrough");
        deleteDialog.setMessage("Are you sure you want do delete?");
        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeleteFromDatabase();
            }
        });
        deleteDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        deleteDialog.show();
    }

    private void ResetProgress() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle("Reset progress");
        deleteDialog.setMessage("Are you sure you want do reset progress?");
        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeleteSessionsFromDatabase();
            }
        });
        deleteDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        deleteDialog.show();
    }

    private void DeleteSessionsFromDatabase() {
        toast = Toast.makeText(ActivityStatsCurrentPlaythrough.this, "Loading...", Toast.LENGTH_LONG);
        querySessions.removeEventListener(childEventListener);
        listOfSessions.clear();
        sessionAdapter.notifyDataSetChanged();
        DatabaseReference deleteReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs").child(gameId);
        deleteReference.child("sessions").setValue(null);
        deleteReference.child("hours").setValue("0");
        deleteReference.child("minutes").setValue("0").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                textViewPlaytimeHoursAndMinutes.setText("00:00");
            }
        });
    }

    private void MarkGameAsFinished() {
        final String[] playstyles = this.getResources().getStringArray(R.array.playstiles);
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Please select your playstyle.");
        dialogBuilder.setSingleChoiceItems(R.array.playstiles, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedPlaystile = playstyles[which];
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StoreInDatabase();
            }
        });

        dialogBuilder.show();
    }

    private void StoreInDatabase() {
        toast = Toast.makeText(ActivityStatsCurrentPlaythrough.this, "Loading...", Toast.LENGTH_LONG);
        toast.show();
        String[] hoursAndMinutes = textViewPlaytimeHoursAndMinutes.getText().toString().split(":");
        DatabaseReference CompletedPlaythroughsReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("FinishedPlaythroughs");
        finishedGameId = UUID.randomUUID().toString();
        CompletedPlaythroughsReference.child(finishedGameId).child("nameFinished").setValue(textViewGameTitle.getText().toString());
        CompletedPlaythroughsReference.child(finishedGameId).child("hours").setValue(hoursAndMinutes[0]);
        CompletedPlaythroughsReference.child(finishedGameId).child("minutes").setValue(hoursAndMinutes[1]);
        CompletedPlaythroughsReference.child(finishedGameId).child("playstyle").setValue(selectedPlaystile).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                StoreFinishedPlaythroughIntoGames();
            }
        });
    }

    private void StoreFinishedPlaythroughIntoGames() {
        final DatabaseReference GamesPlaythroughsReference = firebaseDatabase.getReference().child("games");
        Query query = GamesPlaythroughsReference.orderByChild("name").equalTo(textViewGameTitle.getText().toString());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String gameId = "";
                for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    gameId = snapshot.getKey();
                }

                String playthroughId = UUID.randomUUID().toString();
                String[] hoursAndMinutes = textViewPlaytimeHoursAndMinutes.getText().toString().split(":");
                GamesPlaythroughsReference.child(gameId).child("playthroughs").child(playthroughId).child("hours").setValue(hoursAndMinutes[0]);
                GamesPlaythroughsReference.child(gameId).child("playthroughs").child(playthroughId).child("minutes").setValue(hoursAndMinutes[1]);
                GamesPlaythroughsReference.child(gameId).child("playthroughs").child(playthroughId).child("playstyle").setValue(selectedPlaystile).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        DeleteFromDatabase();
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void DeleteFromDatabase() {
        toast = Toast.makeText(ActivityStatsCurrentPlaythrough.this, "Loading...", Toast.LENGTH_LONG);
        querySessions.removeEventListener(childEventListener);
        DatabaseReference deleteReference = firebaseDatabase.getReference().child("users").child(firebaseUser.getUid()).child("CurrentPlaythroughs").child(gameId);
        deleteReference.setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                toast.cancel();
                StartFinishedPlaythroughStatsActivity();
            }
        });
    }

    private void StartFinishedPlaythroughStatsActivity() {
        Intent intent = new Intent(this, ActivityStatsFinishedPlaythrough.class);
        intent.putExtra("Id", finishedGameId);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        querySessions.removeEventListener(childEventListener);
        super.onBackPressed();
    }
}
